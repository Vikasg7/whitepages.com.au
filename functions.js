(function () {

   String.prototype.strip = function () {
      return this.replace(/\s+/g, " ")
   }

   var deasync = require("deasync")
   var fs = require("fs")
   var request = require("request").defaults({
         jar: true
      }) // Jar true doesn't help in keeping the cookies intact in the next request with the same handle
   var whacko = require("whacko")

   module.exports = {

      fs: fs,

      sleep: deasync.sleep,

      browse: deasync(function (options, cb) {

         if (options.headers) {
            options.headers["User-Agent"] = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.97 Safari/537.36"
         } else {
            options.headers = {
               "User-Agent": "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.97 Safari/537.36"
            }
         }

         request(options, function (err, resp, body) {
            if (err) {
               cb(null, {
                  err: err.code
               })
            } else if (resp.statusCode !== 200) {
               cb(null, {
                  err: resp.statusCode + " - " + resp.statusMessage
               })
            } else {
               cb(null, {
                  body: body,
                  parsed: whacko.load(body)
               })
            }
         })
      }),

      writer: function (fPath, headers) {
         var csv = require("csv-write-stream")
         var fs = require("fs")
         var writer = csv({
            headers: headers
         })
         writer.pipe(fs.createWriteStream(fPath))
         return writer
      },

      logger: {
         urlsDone: 0,
         totUrls: 0,
         lastError: "",
         errCount: 0,
         doneWorkers: 0,
         hangedWorkers: 0,
         workerError: "",
         totWorkers: 0,

         log: function () {
            process.stdout.moveCursor(0, -8)
            this._log()
         },

         _log: function () {
            process.stdout.cursorTo(0)
            process.stdout.clearScreenDown()
            var stdMsg = [
               "Progress      : ", this.urlsDone + " / " + this.totUrls,
               "\n%Completion   : ", parseInt(((this.urlsDone + this.errCount) * 100) / this.totUrls) + "%",
               "\nLast Error    : ", this.lastError,
               "\nErr Count     : ", this.errCount,
               "\nTotal Workers : ", this.totWorkers,
               "\nWorkers Done  : ", this.doneWorkers,
               "\nHanged Workers: ", this.hangedWorkers,
               "\nWorkers Error : ", this.workerError,
               "\nTime used     : ", this.getTime(process.uptime())
            ]
            process.stdout.write(stdMsg.join(""))
         },

         logStatus: function (statusText) {
            process.stdout.moveCursor(0, -8)
            process.stdout.cursorTo(0)
            process.stdout.clearScreenDown()
            process.stdout.write(statusText + "\n")
            this._log()
         },

         getTime: function (seconds) {
            var date = new Date(null)
            date.setSeconds(seconds)
            return date.toUTCString().match(/[0-9]{2}\:[0-9]{2}\:[0-9]{2}/g)[0]
         }
      },

      initWorker: function (urls, writer, logger, scraperCount, delayInSec) {
         var fork = require("child_process").fork
         var worker = fork("./worker.js", [delayInSec])

         worker.on("message", function (msg) {
            if (msg.action === "sendNext") {
               sendNext()
            } else if (msg.action === "writeToCsv") {
               ++logger.urlsDone;
               logger.log()
                  // Writing data to the output sheet
               msg.data.forEach(function (row, i) {
                  writer.write(row)
               })
               sendNext()
            } else if (msg.action === "logError") {
               logger.lastError = msg.errText;
               logger.log();
               fs.appendFileSync("./errCounters.txt", msg.url + "\n");
               ++logger.errCount;
               logger.log()
               sendNext()
            }

            function sendNext() {
               if (urls.length) {
                  worker.send({
                     action: "scrapeIt",
                     url: urls.shift()
                  })
               } else {
                  worker.send({
                     action: "killWorker"
                  })
               }
            }
         })

         worker.on("exit", function () {
            ++logger.doneWorkers;
            logger.log()
            if (logger.doneWorkers === scraperCount) {
               console.log("\nFinished!!")
            }
         })

         worker.on("error", function (err) {
            logger.workerError = err.toString();
            logger.log();
            ++logger.hangedWorkers;
            logger.log()
         })
      }
   }

})()