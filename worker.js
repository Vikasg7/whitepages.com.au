(function () {
   
   var defs = require("./defs.js")
   var delayInSec = process.argv[2]
   
   process.send({action: "sendNext"})

   process.on("message", function (msg) {
      if (msg.action === "scrapeIt") {
         defs.scrape(msg.url, delayInSec)
      } else if (msg.action === "killWorker") {
         process.exit()
      }
   })

})()