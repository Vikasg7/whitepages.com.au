(function () {

   var args = require("yargs")
      .usage("Usage: node init [Options]")
      .demand(["i", "o", "p", "f", "d", "s"])
      .alias("i", "in")
      .describe("i", "File path to input text file.\nUse './' for current folder path")
      .default("i", "")
      .alias("o", "out")
      .describe("o", "File path  to output csv file with .csv extension.")
      .default("o", "")
      .alias("f", "from")
      .describe("f", "Serial number of line to start from.")
      .default("f", 0)
      .alias("p", "processCount")
      .describe("p", "No. of workers to start.")
      .default("p", 1)
      .alias("d", "delay")
      .describe("d", "Delay in no. of seconds")
      .default("d", 5)
      .alias("s", "suburb")
      .describe("s", "Mention the town/suburb for eg. perth-6000. Use '-' for space.")
      .default("s", "")
      .help("h")
      .alias("h", "help")
      .epilog("Copyright 2016 Vikas Gautam")
      .argv

   var imports = require("./functions.js")
   var defs = require("./defs.js")
   var logger = imports.logger
   var path = require("path")

   if (args.o !== "") {
      var oFile = "./output/" + args.o.replace(".csv", "") + "_" + args.f + ".csv"
   } else {
      if (args.s) {
         var oFile = "./output/" + args.s.replace(/ /g, "-") + "_" + args.f + ".csv"
      } else if (args.i) {
         var fileName = path.parse(args.i).name
         var oFile = "./output/" + fileName.replace(/ /g, "-") + "_" + args.f + ".csv"
      }
   }

   console.log("\n");
   logger.log()

   // Getting links
   if (!(args.s || args.i)) {
      logger.logStatus("Error:- There should either a -s or -i flag present.")
      process.exit()
   }
   var urls = defs.makeUrls(args.s, args.i, args.d)

   // Scraping links
   logger.logStatus("Scraping urls...")
   logger.totUrls = urls.length;
   logger.urlsDone = args.f;
   logger.log()
   urls = urls.slice(args.f)

   var writer = imports.writer(oFile)
      // Starting workers
   for (var x = 1; x <= args.p; x++) {
      imports.initWorker(urls, writer, logger, args.p, args.d)
      urls.length && ++logger.totWorkers;
      logger.log()
   }

})()