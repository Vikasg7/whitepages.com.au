(function () {

   String.prototype.strip = function () {
      return this.replace(/\s{2,}/g, " ")
   }
   var imports = require("./functions.js")
   var fs = imports.fs
   var sleep = imports.sleep
   var URL = require("url")
   var browse = imports.browse
   var logger = imports.logger

   var headers = {
      "Host": "www.whitepages.com.au",
      "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
      "Accept-Language": "en-US,en;q=0.5",
      "Accept-Encoding": "gzip, deflate, br",
      "Connection": "keep-alive"
   }

   module.exports = {

      makeUrls: function (subUrb, fPath, delayInSec) {
         if (subUrb) {
            // Make input list here
            var urls = getApiUrls(subUrb, delayInSec)
            logger.logStatus("Done!, " + urls.length + " links found. File saved as ./input/" + subUrb + ".txt")
            fs.writeFileSync("./input/" + subUrb + ".txt", urls.join("\n"), "UTF-8")
            return urls
         } else if (fPath) {
            return fs.readFileSync(fPath, "UTF-8").trim().split("\n")
         }
      },

      scrape: function (url, delayInSec) {
         try {
            var resp = browse({
               url: url,
               headers: headers,
               gzip: true
            })
            if (resp.error) {
               process.send({
                  action: "logError",
                  errText: resp.error,
                  url: url
               })
            } else {
               var rows = []

               // Write you scraper here
               var entry = JSON.parse(resp.body).entry
               var company = {}

               company.companyName = entry.business.name ? entry.business.name : ""
               company.profileId = entry.business.profileId ? entry.business.profileId : ""
               if (entry.presences.length > 0) {
                  var presence = entry.presences[0]
                  var physicalAddress = presence.physicalAddress
                  company.floor = physicalAddress.floor ? physicalAddress.floor : ""
                  company.streetNumber = physicalAddress.streetNumber ? physicalAddress.streetNumber : ""
                  company.streetName = physicalAddress.streetName ? physicalAddress.streetName : ""
                  company.streetType = physicalAddress.streetType ? physicalAddress.streetType : ""
                  company.suburb = physicalAddress.suburb ? physicalAddress.suburb : ""
                  company.state = physicalAddress.state ? physicalAddress.state : ""
                  company.country = physicalAddress.country ? physicalAddress.country : ""
                  company.postcode = physicalAddress.postcode ? physicalAddress.postcode : ""
                  company.PhoneValue = ""
                  company.PhoneDisplay = ""
                  company.UrlValue = ""
                  company.UrlDisplay = ""

                  if (presence.contacts.length === 1) {
                     if (presence.contacts[0].contactType === "PHONE") {
                        company.PhoneValue = presence.contacts[0].actualValue + " "
                        company.PhoneDisplay = presence.contacts[0].displayValue + " "
                     }
                  } else if (presence.contacts.length === 2) {
                     if (presence.contacts[1].contactType === "URL") {
                        company.UrlValue = presence.contacts[1].actualValue + " "
                        company.UrlDisplay = presence.contacts[1].displayValue + " "
                     }
                  }

                  // Google url for manual search
                  var googleUrlParsed = URL.parse("https://www.google.com.au/search")
                  googleUrlParsed.query = {
                     q: [
                        company.companyName,
                        company.streetName,
                        company.streetType,
                        company.suburb,
                        company.state,
                        company.postcode
                     ].join(" ").strip()
                  }
                  company.googleURL = URL.format(googleUrlParsed)
                  
                  // googleURL2
                  googleUrlParsed.query = {
                     q: [
                        company.companyName,
                        company.suburb,
                        "inurl:contact"
                     ].join(" ").strip()
                  }

                  company.googleURL2 = URL.format(googleUrlParsed)

               }
               rows.push(company)
               sleep(delayInSec * 1000)
               process.send({
                  action: "writeToCsv",
                  data: rows
               })
            }
         } catch (e) {
            sleep(delayInSec * 1000)
            process.send({
               action: "logError",
               errText: e.toString(),
               url: url
            })
         }
      }
   }

   function getApiUrls(subUrb, delayInSec) {
      logger.logStatus("Getting company urls...")
      var links = []
         // Getting list of Alphabets
      var alphabetsURL = ["https://www.whitepages.com.au/api/index/location/", subUrb, "/firstLetterOfBusinessName"].join("")
      var alphabetsResp = browse({
         url: alphabetsURL,
         method: "GET",
         headers: headers,
         gzip: true
      })
      if (alphabetsResp.err) {
         logger.lastError = alphabetsResp.err;
         ++logger.errCount;
         logger.log();
         process.exit()
      } else {
         var jsonResp = JSON.parse(alphabetsResp.body)
         if (!jsonResp.results.length) {
            logger.lastError = "No Alphabet to scrape, exiting...";
            ++logger.errCount;
            logger.log();
            process.exit()
         } else {
            var alphabets = jsonResp.results
               // Looping through list of alphabets
            alphabets.forEach(getLinks)
         }
      }

      return links

      function getLinks(alphabet, i) {
         var pageNum = 1
            // Gettting company links
         var alphaURL = ["https://www.whitepages.com.au/api/index/location/", alphabet.toUpperCase(), "/entries?page=", pageNum, "&resolvedSuburb=", subUrb].join("")
         var alphaResp = browse({
            url: alphaURL,
            method: "GET",
            headers: headers,
            gzip: true
         })
         if (alphaResp.err) {
            logger.lastError = alphaResp.err;
            ++logger.errCount;
            logger.log();
            process.exit()
         } else {
            // Making API links
            var alphaJson = JSON.parse(alphaResp.body)
            makeLinks(alphaJson)

            // Taking care of pagination
            if (alphaJson.totalPages > 1) {
               sleep(delayInSec * 1000)
               while (pageNum !== alphaJson.totalPages) {
                  pageNum++
                  alphaURL = ["https://www.whitepages.com.au/api/index/location/", alphabet.toUpperCase(), "/entries?page=", pageNum, "&resolvedSuburb=", subUrb].join("")
                  alphaResp = browse({
                     url: alphaURL,
                     method: "GET",
                     headers: headers,
                     gzip: true
                  })
                  if (alphaResp.err) {
                     logger.lastError = alphaResp.err;
                     ++logger.errCount;
                     logger.log();
                     process.exit()
                  } else {
                     alphaJson = JSON.parse(alphaResp.body)
                     makeLinks(alphaJson)
                  }
                  sleep(delayInSec * 1000)
               }
            }

         }
         sleep(delayInSec * 1000)
      }

      function makeLinks(json) {
         json.results.forEach(function (result, i) {
            var link = ["https://www.whitepages.com.au/api/b/presence/"]
            result.presences.forEach(function (presence, i) {
               var link_ = link.slice(0)
               link_.push(presence.profileId)
               links.push(link_.join("").toLowerCase())
               logger.totUrls++;
               logger.log()
            })
         })
      }
   }

})()